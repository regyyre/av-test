import pika
import sys
import os
import time
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib

amqp_url = os.environ['AMQP_URL']
email_password = os.environ['EMAIL_PASSWORD']
target_email = os.environ['TARGET_EMAIL']

def send_email(target, message):
    msg = MIMEMultipart()

    message = "Something went wrong with parser. The file {} is not a text".format(message)

    # setup the parameters of the message
    password = email_password
    msg['From'] = "alert.noreply.avtest@gmail.com"
    msg['To'] = target
    msg['Subject'] = "Notify"

    msg.attach(MIMEText(message, 'plain'))

    server = smtplib.SMTP('smtp.gmail.com: 587')

    server.starttls()

    server.login(msg['From'], password)

    server.sendmail(msg['From'], msg['To'], msg.as_string())

    server.quit()

    print("successfully sent email to %s:" % (msg['To']))

connection = pika.BlockingConnection(
    pika.URLParameters(amqp_url))
channel = connection.channel()

channel.queue_declare(queue='Errors', durable=True)

def callback(ch, method, properties, body):
    print(" [x] Received %r" % body.decode())
    send_email(target_email, body.decode())
    time.sleep(body.count(b'.'))
    print(" [x] Done")
    ch.basic_ack(delivery_tag=method.delivery_tag)


channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue='Errors', on_message_callback=callback)

channel.start_consuming()