import sys
import random
import requests
from bs4 import BeautifulSoup


def get_site(url):
    if url[0:7] != 'http://' and url[0:8] != 'https://':
        url = 'http://'+url

    page = requests.get(url)
    soup = BeautifulSoup(page.content, 'html.parser')
    text = soup.find_all(text=True)

    output = ''
    blacklist = [
        '[document]',
        'noscript',
        'header',
        'html',
        'meta',
        'head',
        'input',
        'script',
        'static'
    ]

    for t in text:
        if t.parent.name not in blacklist:
            output += '{} '.format(t)

    with open('folder/'+url[url.find('/')+2:].replace('/', '-')+'.txt', 'w', encoding='utf-8') as file:
        file.write(output)

try:
    if len(sys.argv)>1:
        get_site(sys.argv[1])
    else:
        with open('folder/'+"".join(random.choices(['a','b','c','d'], k=15))+'.txt', 'w', encoding='utf-8') as file:
            file.write("".join(random.choices(['a','b','c','d','1', '  '], k=300)))
except:
    print("something went wrong")