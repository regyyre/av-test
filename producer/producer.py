import pika
import os
import glob
import time
import magic

amqp_url = os.environ['AMQP_URL']

ls = glob.glob('/usr/src/app/folder/*', recursive=True)
file_types = ['text/plain']

def send(amqp_host,queue, message):
    connection = pika.BlockingConnection(
        pika.URLParameters(amqp_host))
    channel = connection.channel()
    channel.basic_publish(exchange='', routing_key=queue, body=message)
    connection.close()

try:
    while True:
        new_ls = set(glob.glob('/usr/src/app/folder/*', recursive=True)) - set(ls)
        if len(new_ls) > 0:
            for loc in new_ls:
                if magic.from_file(loc, mime=True, ) in file_types:
                    with open(loc, 'r', encoding='UTF-8') as file:
                        send(amqp_url, "Parser", loc + '\n' + file.read())
                else:
                    send(amqp_url, "Errors", loc)
                print("loc", loc, '|', magic.from_file(loc, mime=True))
            print(new_ls)
            ls = glob.glob('/usr/src/app/folder/*', recursive=True)
        else:
            pass
        time.sleep(5)
except KeyboardInterrupt:
    pass