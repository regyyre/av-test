import pymysql
import os
import time
import csv

def exec_sql(query):
    connection = pymysql.connect(host='db',
                                 port=3306,
                                 user='root',
                                 password='password',
                                 database='av',
                                 cursorclass=pymysql.cursors.DictCursor)

    with connection:
        with connection.cursor() as cursor:
            cursor.execute(query)
            values = cursor.fetchall()
        connection.commit()
    return values

def write_line_csv(line, file):
    with open(file, 'a', newline='') as csvfile:
        csv_writer = csv.writer(csvfile, delimiter=',')
        csv_writer.writerow(line)

try:
    while True:
        over_values = exec_sql(f"SELECT * FROM wordcount WHERE count > {os.environ['MAX_WORD_COUNT']};")
        if len(over_values):
            query_body = "("
            for row in over_values:
                write_line_csv(list(row.values()),'/usr/src/app/data/avtest.csv')
                query_body = query_body + '\'' + row['word'] + '\', '
            query_body = query_body[:-2] + ')'
            exec_sql(f"DELETE FROM wordcount WHERE word IN {query_body}")
        time.sleep(20)
except KeyboardInterrupt:
    pass