import pika
import os
from collections import Counter
import pymysql.cursors

amqp_url = os.environ['AMQP_URL']

connection = pika.BlockingConnection(
    pika.URLParameters(amqp_url))
channel = connection.channel()

channel.queue_declare(queue='Parser', durable=True)

def exec_sql(query):
    connection = pymysql.connect(host='db',
                                 port=3306,
                                 user='root',
                                 password='password',
                                 database='av',
                                 cursorclass=pymysql.cursors.DictCursor)

    with connection:
        with connection.cursor() as cursor:
            cursor.execute(query)

        connection.commit()

def callback(ch, method, properties, body):
    print(" [x] Received")
    text = body.decode().split()
    loc = text.pop(0)
    print(loc)
    wordcount = Counter(text)
    query_values = ""
    file_name = str(loc[loc.rfind('/'):])
    for i in wordcount.items():
        query_values = query_values[:-1] + str(i)[:-1] + ', \'' + file_name[1:] + ', \'), '
    exec_sql(f"INSERT INTO wordcount (word,count,file_names) VALUES {query_values[:-2]} AS new(w,c,n) ON DUPLICATE KEY UPDATE count = count + c, file_names = CONCAT(file_names,n)")
    print(" [x] Done")
    ch.basic_ack(delivery_tag=method.delivery_tag)

exec_sql("CREATE TABLE IF NOT EXISTS wordcount (word VARCHAR(254), count INT(11), file_names TEXT, UNIQUE (word));") #Table init
channel.basic_qos(prefetch_count=40)
channel.basic_consume(queue='Parser', on_message_callback=callback)

channel.start_consuming()